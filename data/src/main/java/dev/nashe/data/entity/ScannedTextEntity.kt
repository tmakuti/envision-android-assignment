package dev.nashe.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "scannedtext")
data class ScannedTextEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L,
    val text : String,
    val savedAt: Date

)