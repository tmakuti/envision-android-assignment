package dev.nashe.data.entity


data class ParagraphEntity (
    val language: String,
    val paragraph: String
)