package dev.nashe.data.repository

import dev.nashe.data.api.ApiService
import dev.nashe.domain.models.text.Paragraph
import dev.nashe.domain.repository.FileUploadRepository
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject

class FileUploadRepositoryImpl @Inject constructor(
    private val service: ApiService
) : FileUploadRepository {

    override suspend fun uploadFile(filePath: String): List<Paragraph> {
        val file = File(filePath)

        val requestPhoto = file.asRequestBody("multipart/form-data".toMediaTypeOrNull())
        val photo = MultipartBody.Part.createFormData("photo", file.name,  requestPhoto)

        return service.uploadFile(photo).response.paragraphs
    }
}