package dev.nashe.data.repository

import dev.nashe.data.db.TextDao
import dev.nashe.data.mapper.ScannedTextDomainMapper
import dev.nashe.data.mapper.ScannedTextEntityMapper
import dev.nashe.domain.models.text.ScannedText
import dev.nashe.domain.repository.TextRepository
import javax.inject.Inject

class TextRepositoryImpl @Inject constructor(
    private val textDao: TextDao,
    private val scannedTextEntityMapper: ScannedTextEntityMapper,
    private val scannedTextDomainMapper: ScannedTextDomainMapper
) : TextRepository {
    override suspend fun getPersistedText(): List<ScannedText> {
        return scannedTextEntityMapper.mapToDomainList(textDao.getScanned())
    }

    override suspend fun insert(text: ScannedText) {
        textDao.saveRecord(scannedTextDomainMapper.mapToEntity(text))
    }
}