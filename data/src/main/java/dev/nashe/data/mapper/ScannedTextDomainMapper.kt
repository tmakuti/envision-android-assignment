package dev.nashe.data.mapper

import dev.nashe.data.entity.ScannedTextEntity
import dev.nashe.data.mapper.base.RequestMapper
import dev.nashe.domain.models.text.ScannedText
import javax.inject.Inject

class ScannedTextDomainMapper @Inject constructor() :
    RequestMapper<ScannedText, ScannedTextEntity> {

    override fun mapToEntity(domain: ScannedText): ScannedTextEntity {
        return ScannedTextEntity(
            text = domain.text,
            savedAt = domain.savedAt
        )
    }
}