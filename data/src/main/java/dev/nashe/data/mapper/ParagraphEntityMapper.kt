package dev.nashe.data.mapper

import dev.nashe.data.entity.ParagraphEntity
import dev.nashe.data.mapper.base.ResponseMapper
import dev.nashe.domain.models.text.Paragraph
import javax.inject.Inject

class ParagraphEntityMapper @Inject constructor() : ResponseMapper<ParagraphEntity, Paragraph> {

    override fun mapToDomain(entity: ParagraphEntity): Paragraph {
        return Paragraph(
            language = entity.language,
            paragraph = entity.paragraph
        )
    }
}