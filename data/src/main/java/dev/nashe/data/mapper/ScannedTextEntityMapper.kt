package dev.nashe.data.mapper

import dev.nashe.data.entity.ScannedTextEntity
import dev.nashe.data.mapper.base.ResponseMapper
import dev.nashe.domain.models.text.ScannedText
import javax.inject.Inject

class ScannedTextEntityMapper @Inject constructor() : ResponseMapper<ScannedTextEntity, ScannedText> {

    override fun mapToDomain(entity: ScannedTextEntity): ScannedText {
        return ScannedText(
            text = entity.text,
            savedAt = entity.savedAt
        )
    }
}