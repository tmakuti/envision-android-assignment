package dev.nashe.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import dev.nashe.data.entity.ParagraphEntity
import dev.nashe.data.entity.ScannedTextEntity

@Dao
interface TextDao {

    @Query("select * from scannedtext order by savedAt desc")
    suspend fun getScanned() : List<ScannedTextEntity>

    @Insert
    suspend fun saveRecord(scannedTextEntity: ScannedTextEntity) : Long

}