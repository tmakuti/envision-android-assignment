package dev.nashe.data.api

import dev.nashe.domain.models.text.ParagraphsResponse
import okhttp3.MultipartBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface ApiService {
    @Multipart
    @POST("readDocument")
    suspend fun uploadFile(@Part photo : MultipartBody.Part) : ParagraphsResponse
}