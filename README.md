# Envision Android Assignment

## Introduction


Envision Library is a feature within the Envision app that users a lot after they've scanned a document. It offers them the convenience of storing the scanned documents within the app in an accessible way. This task will be a simple re-implementation of that feature.

## Design


You'll find the design for this feature through this figma link:  https://www.figma.com/file/m2AMO1eRV6SqWje63WmY2a/Envision-App-Assignments?node-id=91%3A1011

Instructions on how to approach the UX of the Assignment is also provided on the figma page itself. 

## Technical Requirements

1. For the OCR setup please use this POST endpoint: https://letsenvision.app/api/test/readDocument. 

Example usage of the API endpoint: 

`curl --location --request POST 'https://letsenvision.app/api/test/readDocument' \
--header 'Cookie: __cfduid=d97604b6c67574ccd048c013ffbee703a1614774197' \
--form 'photo=@/Users/johndoe/Desktop/Screenshot 2021-02-25 at 23.50.47.png'`

For more details on the API, here's the Postman [link](https://www.getpostman.com/collections/771c175ea7a0e2db34b9). 


2. Make sure that the app is entirely accessible using Talkback. If you're not familar with Android Accessibility, this is a good place to start: https://developer.android.com/codelabs/starting-android-accessibility

3. CameraX should be used for the camera implementation. 

4. Library files will only have to be saved locally for the time being. 

## Submission

This repo should be forked and submitted with the Assignment. The assignment itself doesn't have a hard time limit and shouldn't take more than a few hours to implement. 

Incase of any questions with regards to the assignment, please write to karthik@letsenvision.com 


# Tinashe Makuti Submission Notes

<p align="center">Min Sdk
  <a href="https://android-arsenal.com/api?level=21"><img alt="API" src="https://img.shields.io/badge/API-21%2B-brightgreen.svg?style=flat"/></a>
</p>

# Product Review App

## Assignment for Android Engineer Role

Hello, Bonjour - This is a repository for an OCR App.

The app is written 95.6% in Kotlin with all the necessary Unit and Instrumentation tests.

As such in this project i aim to demonstrate:

* Clean Architecture with MVVM (Model View ViewModel) on the presentation layer
* Use of Jetpack libraries
* Use of Kotlin's Coroutines and Flow for background execution
* Dependency Injection using Dagger Hilt
* Persistence with room.
* Clean UI with material design.

## Prerequisites

In order to run this project you need the following:
- Android Studio 4.1.1 or better
- Gradle 6.5 or better
- JDK 1.8
- [Android SDK](https://developer.android.com/studio/index.html)

## How i went about it

<img src="https://blog.cleancoder.com/uncle-bob/images/2012-08-13-the-clean-architecture/CleanArchitecture.jpg"/>
<br>

Its practially improbable to have it all in order, as disorder is the natural state of order. However in this project i tried keep a distinct seperation of. concerns, that in turn brings a level of structural sanity to the codebase. As shown in the pictorial illustration of clean architecture, this app is divided into three sub modules - Domain, Data and App.

### Domain Layer

The domain layer contains UseCase / Interactor instances that are used to collect data from the Data Layer and pass it to the Presentation Layer (App). As such in our context we have four UseCases `GetPersistedText`, `SaveScannedText`, `UploadFile`. Following the dependency inversion principle (DIP) that higher level modules should not depend on low level modules, this layer does not have any mapper classes.

All the interfaces to be used in the data layer were defined here, also in accordance with the DIP principle, this is to make sure that all the modules depend on abstractions.

### Data Layer

The data layer is responsible for the data that will be fed to the entire application, in our case we had to fetch data from a remote source and cache it locally so that we the app can still function offline. As an agreement to the contract shared by the domain layer, concrete classes implementing the interfaces reside in this layer. With the aid of mapping we are able to have an exchange in abstractions between the two layers.

The networking here is performed by `Retrofit`. A popular Http client for andriod. Retrofit depends on `OkHttp` to make requests.
`Retrofit` also has builtin support for `Coroutines` hence my choice in the library. Caching and persistence was done via `Room`

### App (The presentation) Layer

This layer has houses all  Android Framework specific tooling i.e User Interface (UI) and  `ViewModels` which bridge the gap between the data abstractions and the UI. The architecture used in this layer was MVVM as it gives a good seperation of concerns. The availalibity of lifecycle aware components also made the decision to go with MVVM an easy one to make. The ViewModels contain a reference to `UseCase` instances, these references are passed through dependency injection.

#### Tooling Used
`Data binding` this is an amazing tool, using binding adapters reduces a lot of boiler plate, thus making views cleaner.
`LiveData` keeps you updated all the time.
`Dagger Hilt` Simple dependency management.

## Tests

Unit Tests were done against the domain layer to demonstrate how we can go about it.

## Libraries I chose to use

* [Kotlin](https://kotlinlang.org/)
* [Kotlin Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html)
* [Retrofit](http://square.github.io/retrofit/) - An http client for android
* [Okhttp](http://square.github.io/okhttp/) - For networking requests
* [Mockito](http://site.mockito.org/) - For mocking instances
* [Dagger Hilt](https://dagger.dev/hilt/) - For dependency injection
* [Truth](https://truth.dev/) - For assertions during testing
* Jetpack Libraries

## Screenshots

<img src="http://nashe.dev/wp-content/uploads/2021/05/envision_2.jpeg"  width="250px"/>  <img src="http://nashe.dev/wp-content/uploads/2021/05/envision_1.jpeg"  width="250px"/><img src="http://nashe.dev/wp-content/uploads/2021/05/envision_3.jpeg"  width="250px"/> <img src="http://nashe.dev/wp-content/uploads/2021/05/envision_4.jpeg"  width="250px"/>



