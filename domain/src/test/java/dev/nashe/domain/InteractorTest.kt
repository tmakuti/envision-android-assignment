package dev.nashe.domain

import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockitokotlin2.whenever
import dev.nashe.domain.interactors.GetPersistedText
import dev.nashe.domain.interactors.SaveScannedText
import dev.nashe.domain.interactors.UploadFile
import dev.nashe.domain.models.text.Paragraph
import dev.nashe.domain.models.text.ScannedText
import dev.nashe.domain.repository.FileUploadRepository
import dev.nashe.domain.repository.TextRepository
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class InteractorTest {

    private lateinit var getPersistedText: GetPersistedText
    private lateinit var saveScannedText: SaveScannedText
    private lateinit var uploadFile: UploadFile

    @Mock
    lateinit var fileUploadRepository: FileUploadRepository

    @Mock
    lateinit var textRepository: TextRepository

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        getPersistedText = GetPersistedText(textRepository)
        saveScannedText = SaveScannedText(textRepository)
        uploadFile = UploadFile(fileUploadRepository)
    }

    private suspend fun stubTextRepositoryGetScannedText(scannedText : ScannedText){
        whenever(getPersistedText()).thenReturn(listOf(scannedText))
    }

    private suspend fun stubTextRepositorySaveScannedText(scannedText: ScannedText){
        whenever(saveScannedText(scannedText)).thenReturn(Unit)
    }

    private suspend fun stubFileUploadRepositoryUploadFile(result : List<Paragraph>){
        whenever(uploadFile(DataStub.filePath)).thenReturn(result)
    }

    @Test
    fun `check if text is retrieved from the repository`() = runBlockingTest {
        stubTextRepositoryGetScannedText(DataStub.scannedText)

        val texts = getPersistedText()

        assertThat(texts).isEqualTo(DataStub.listScannedText)
    }

    @Test
    fun `check if scannedText is being saved`() = runBlockingTest {
        stubTextRepositorySaveScannedText(DataStub.scannedText)

        val unitRes = saveScannedText(DataStub.scannedText)

        assertThat(unitRes).isEqualTo(Unit)
    }

    @Test
    fun `check if file is uploaded`() =  runBlockingTest {
        stubFileUploadRepositoryUploadFile(DataStub.listOfParagraph)

        val paragraphs = uploadFile(DataStub.filePath)

        assertThat(paragraphs).isEqualTo(DataStub.listOfParagraph)
    }

}