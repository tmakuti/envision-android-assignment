package dev.nashe.domain

import dev.nashe.domain.models.text.Paragraph
import dev.nashe.domain.models.text.ScannedText
import java.util.*

internal object DataStub {
    val scannedText = ScannedText("en-US", Date())
    val paragraph = Paragraph("en", "Tinashe")
    val listScannedText = listOf(scannedText)
    val listOfParagraph = listOf(paragraph)
    val filePath = "some random path"
}