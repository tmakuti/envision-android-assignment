package dev.nashe.domain.repository

import dev.nashe.domain.models.text.Paragraph

interface FileUploadRepository {
    suspend fun uploadFile(filePath : String) : List<Paragraph>
}