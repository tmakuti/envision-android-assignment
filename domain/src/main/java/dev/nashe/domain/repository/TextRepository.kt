package dev.nashe.domain.repository

import dev.nashe.domain.models.text.ScannedText

interface TextRepository {
    suspend fun getPersistedText()  : List<ScannedText>

    suspend fun insert(text: ScannedText)
}