package dev.nashe.domain.interactors

import dev.nashe.domain.repository.TextRepository
import javax.inject.Inject

class GetPersistedText @Inject constructor(
    private val repository: TextRepository
) {
    suspend operator fun invoke() = repository.getPersistedText()
}