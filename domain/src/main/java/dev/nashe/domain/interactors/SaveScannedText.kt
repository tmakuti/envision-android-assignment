package dev.nashe.domain.interactors

import dev.nashe.domain.models.text.ScannedText
import dev.nashe.domain.repository.TextRepository
import javax.inject.Inject

class SaveScannedText @Inject constructor(
    private val repository: TextRepository
) {
    suspend operator fun invoke(text: ScannedText) = repository.insert(text)
}