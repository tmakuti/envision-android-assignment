package dev.nashe.domain.interactors

import dev.nashe.domain.repository.FileUploadRepository
import javax.inject.Inject

class UploadFile @Inject constructor(
    private val fileUploadRepository: FileUploadRepository
){
    suspend operator fun invoke(filePath : String) = fileUploadRepository.uploadFile(filePath)
}