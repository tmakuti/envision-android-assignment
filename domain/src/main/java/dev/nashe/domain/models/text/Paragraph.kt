package dev.nashe.domain.models.text

data class Paragraph(
    val language: String,
    val paragraph: String
)