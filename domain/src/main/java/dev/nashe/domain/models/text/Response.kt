package dev.nashe.domain.models.text


data class Response(
    val paragraphs: List<Paragraph>
)