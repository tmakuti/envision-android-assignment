package dev.nashe.domain.models.text

data class ParagraphsResponse(
    val response: Response
)