package dev.nashe.domain.models.text

import java.util.*

data class ScannedText (
    val text : String,
    val savedAt: Date
)