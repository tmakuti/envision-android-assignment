package dev.nashe.envisionandroidassignment.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dev.nashe.data.BuildConfig
import dev.nashe.data.api.ApiService
import dev.nashe.data.api.RetrofitServiceFactory
import dev.nashe.data.db.AppDatabase
import dev.nashe.data.db.TextDao
import dev.nashe.data.repository.FileUploadRepositoryImpl
import dev.nashe.data.repository.TextRepositoryImpl
import dev.nashe.domain.repository.FileUploadRepository
import dev.nashe.domain.repository.TextRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DataModule {

    companion object {
        @Provides
        @Singleton
        fun providesApiService(): ApiService {
            return RetrofitServiceFactory.makeRetrofitApiService(BuildConfig.DEBUG)
        }

        @Provides
        @Singleton
        fun providesAppDatabase(@ApplicationContext context: Context) : AppDatabase {
            return AppDatabase.getDatabase(context)
        }

        @Singleton
        @Provides
        fun provideTextDao(appDatabase: AppDatabase): TextDao {
            return appDatabase.textDao()
        }

        @Provides
        @Singleton
        fun providesFileUploadRepository(fileUploadRepository: FileUploadRepositoryImpl): FileUploadRepository = fileUploadRepository

        @Provides
        @Singleton
        fun providesTextRepository(textRepository: TextRepositoryImpl): TextRepository = textRepository

    }
}