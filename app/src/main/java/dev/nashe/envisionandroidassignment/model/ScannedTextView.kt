package dev.nashe.envisionandroidassignment.model

import java.util.*

data class ScannedTextView (
    val text : String,
    val savedAt : Date
)