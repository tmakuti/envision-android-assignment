package dev.nashe.envisionandroidassignment.model

data class ParagraphView (
    val language: String,
    val paragraph: String
)