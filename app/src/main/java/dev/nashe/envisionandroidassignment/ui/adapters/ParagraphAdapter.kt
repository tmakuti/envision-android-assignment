package dev.nashe.envisionandroidassignment.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import dev.nashe.envisionandroidassignment.databinding.ItemParagraphBinding
import dev.nashe.envisionandroidassignment.model.ParagraphView
import dev.nashe.envisionandroidassignment.ui.adapters.base.BaseRecyclerAdapter
import dev.nashe.envisionandroidassignment.ui.viewholder.ParagraphViewHolder

class ParagraphAdapter(callback: Callback<ParagraphView>) :
        BaseRecyclerAdapter<ParagraphView, ParagraphViewHolder>(callback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParagraphViewHolder {
        return ParagraphViewHolder(ItemParagraphBinding.inflate(LayoutInflater.from(parent.context)))
    }
}