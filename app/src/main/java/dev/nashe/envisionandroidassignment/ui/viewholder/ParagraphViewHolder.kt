package dev.nashe.envisionandroidassignment.ui.viewholder

import androidx.recyclerview.widget.RecyclerView
import dev.nashe.envisionandroidassignment.databinding.ItemParagraphBinding
import dev.nashe.envisionandroidassignment.model.ParagraphView
import dev.nashe.envisionandroidassignment.ui.adapters.base.BaseRecyclerAdapter
import dev.nashe.envisionandroidassignment.ui.viewholder.base.BaseViewHolder

class ParagraphViewHolder(binding: ItemParagraphBinding) :
        BaseViewHolder<ParagraphView, ItemParagraphBinding, BaseRecyclerAdapter.Callback<ParagraphView>>(
                binding
        ) {

    override fun doOnBind(item: ParagraphView, callback: BaseRecyclerAdapter.Callback<ParagraphView>?) {
        binding.paragraph = item

        val rootView = binding.root
        rootView.layoutParams = RecyclerView.LayoutParams(
                RecyclerView.LayoutParams.MATCH_PARENT,
                RecyclerView.LayoutParams.WRAP_CONTENT
        )

        if (callback != null) {
            binding.root.setOnClickListener { callback.onItemSelected(item) }
        }
    }
}