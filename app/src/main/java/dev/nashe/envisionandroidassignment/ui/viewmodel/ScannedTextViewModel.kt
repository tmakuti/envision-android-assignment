package dev.nashe.envisionandroidassignment.ui.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.nashe.domain.interactors.GetPersistedText
import dev.nashe.domain.interactors.SaveScannedText
import dev.nashe.envisionandroidassignment.mapper.ScannedTextDomainMapper
import dev.nashe.envisionandroidassignment.mapper.ScannedTextViewMapper
import dev.nashe.envisionandroidassignment.model.ScannedTextView
import javax.inject.Inject
import dev.nashe.envisionandroidassignment.utils.Result
import kotlinx.coroutines.launch

@HiltViewModel
class ScannedTextViewModel @Inject constructor(
    private val getPersistedText: GetPersistedText,
    private val saveScannedText: SaveScannedText,
    private val scannedTextViewMapper: ScannedTextViewMapper,
    private val scannedTextDomainMapper: ScannedTextDomainMapper
) : ViewModel() {

    private val _scannedTextLiveData = MutableLiveData<Result<List<ScannedTextView>>>()
    val scannedTextLiveData: LiveData<Result<List<ScannedTextView>>>
        get() = _scannedTextLiveData

    fun getAllScannedText(){
        viewModelScope.launch {
            _scannedTextLiveData.value = Result.Loading
            try {
                val scannedTextListing = scannedTextViewMapper.mapToViewList(getPersistedText())
                _scannedTextLiveData.value = Result.Success(scannedTextListing)

            } catch (e: Exception) {
                _scannedTextLiveData.value = Result.Error(e.message)
            }
        }
    }

    fun persistScannedText(scannedTextView: ScannedTextView){
        viewModelScope.launch {
            saveScannedText(scannedTextDomainMapper.mapToDomain(scannedTextView))
        }
    }
}