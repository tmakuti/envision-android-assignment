package dev.nashe.envisionandroidassignment.ui.activities

import android.graphics.Color
import android.os.Bundle
import android.widget.EditText
import android.widget.FrameLayout
import androidx.activity.viewModels
import androidx.fragment.app.activityViewModels
import androidx.viewpager.widget.ViewPager
import dagger.hilt.android.AndroidEntryPoint
import dev.nashe.envisionandroidassignment.R
import dev.nashe.envisionandroidassignment.databinding.ActivityHomeBinding
import dev.nashe.envisionandroidassignment.ui.activities.base.BaseActivity
import dev.nashe.envisionandroidassignment.ui.adapters.TabFragmentAdapter
import dev.nashe.envisionandroidassignment.ui.fragments.CaptureFragment
import dev.nashe.envisionandroidassignment.ui.fragments.LibraryFragment
import dev.nashe.envisionandroidassignment.ui.viewmodel.ScannedTextViewModel
import dev.nashe.envisionandroidassignment.utils.CustomScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeActivity : BaseActivity<ActivityHomeBinding>(), CoroutineScope by CustomScope() {

    override val layout: Int
        get() = R.layout.activity_home

    private val viewModel : ScannedTextViewModel by viewModels ()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val adapter = TabFragmentAdapter(supportFragmentManager)
        adapter.addFragment(CaptureFragment(), getString(R.string.title_capture))
        adapter.addFragment(LibraryFragment(), getString(R.string.title_library))

        with(binding?.viewPager) {
            this?.adapter = adapter
            this?.addOnPageChangeListener(onPageChangeListener)
            binding?.tabs?.setupWithViewPager(this)
            binding?.tabs?.setTabTextColors(Color.parseColor("#f5f5f5"), Color.parseColor("#ffffff"))
        }
    }

    private val onPageChangeListener: ViewPager.SimpleOnPageChangeListener = object : ViewPager.SimpleOnPageChangeListener() {

        override fun onPageScrolled(i: Int, positionOffset: Float, positionOffsetPx: Int) {

        }

        override fun onPageSelected(i: Int) {
            if (i == MAGIC_NUMBER){
                launch (Dispatchers.IO) {
                    viewModel.getAllScannedText()
                }
            }
        }

        override fun onPageScrollStateChanged(i: Int) {

        }
    }

    fun navigateToLibrary(){
        binding?.apply {
            viewPager.currentItem += MAGIC_NUMBER
        }
    }

    companion object {
        const val MAGIC_NUMBER = 1
    }
}