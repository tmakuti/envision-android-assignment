package dev.nashe.envisionandroidassignment.ui.fragments

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.common.util.concurrent.ListenableFuture
import dagger.hilt.android.AndroidEntryPoint
import dev.nashe.envisionandroidassignment.R
import dev.nashe.envisionandroidassignment.databinding.FragmentCaptureBinding
import dev.nashe.envisionandroidassignment.model.ParagraphView
import dev.nashe.envisionandroidassignment.model.ScannedTextView
import dev.nashe.envisionandroidassignment.ui.activities.HomeActivity
import dev.nashe.envisionandroidassignment.ui.adapters.ParagraphAdapter
import dev.nashe.envisionandroidassignment.ui.adapters.base.BaseRecyclerAdapter
import dev.nashe.envisionandroidassignment.ui.fragments.base.BaseFragment
import dev.nashe.envisionandroidassignment.ui.viewmodel.ScanPhotoViewModel
import dev.nashe.envisionandroidassignment.ui.viewmodel.ScannedTextViewModel
import dev.nashe.envisionandroidassignment.utils.Result
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@AndroidEntryPoint
class CaptureFragment : BaseFragment<FragmentCaptureBinding>(), BaseRecyclerAdapter.Callback<ParagraphView>, View.OnClickListener {

    override val layout: Int
        get() = R.layout.fragment_capture

    private val viewModel: ScanPhotoViewModel by viewModels()
    private val persistenceViewModel: ScannedTextViewModel by viewModels()
    private val paragraphAdapter: ParagraphAdapter = ParagraphAdapter(this)
    private var paragraphs: List<ParagraphView>? = null

    private var imageCapture: ImageCapture? = null

    private lateinit var safeContext: Context

    private lateinit var outputDirectory: File
    private lateinit var cameraExecutor: ExecutorService

    override fun onAttach(context: Context) {
        super.onAttach(context)
        safeContext = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (allPermissionsGranted()) {
            startCamera()
        } else {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_CODE_PERMISSIONS)
        }

        binding?.apply {
            initDashboardView()
            observeParagraphs()
            btnCapture.setOnClickListener {
                takePhoto()
            }
        }

        binding?.listener = this

        outputDirectory = getOutputDirectory()

        cameraExecutor = Executors.newSingleThreadExecutor()
    }

    private fun observeParagraphs() {
        viewModel.detectedTextLiveData.observe(viewLifecycleOwner, {
            when (it) {
                is Result.Loading -> {
                    binding?.tvLoading?.visibility = View.VISIBLE
                }
                is Result.Error -> {
                    Snackbar.make(requireView(), "An error occured while loading", Snackbar.LENGTH_LONG).show()
                    binding?.tvLoading?.visibility = View.GONE
                }
                is Result.Success -> {
                    if (it.data.isNotEmpty()) {
                        binding?.apply {
                            grpText.visibility = View.VISIBLE
                            grPhotos.visibility = View.GONE
                            binding?.tvLoading?.visibility = View.GONE
                        }
                        paragraphAdapter.setItems(it.data)
                        paragraphs = it.data
                    }
                }
                else -> {
                    Log.d(TAG, "Illegal state")
                }
            }
        })
    }

    private fun initDashboardView() {
        binding!!.rvTextListing.apply {
            adapter = paragraphAdapter
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }
    }

    private fun bindUseCases() {
        imageCapture = ImageCapture.Builder()
                .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                .build()

        val cameraProviderFuture: ListenableFuture<ProcessCameraProvider> = ProcessCameraProvider.getInstance(safeContext)
        val processCameraProvider = cameraProviderFuture.get()
        processCameraProvider.bindToLifecycle(this, CameraSelector.DEFAULT_BACK_CAMERA, imageCapture)
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

        cameraProviderFuture.addListener(Runnable {
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            val preview = Preview.Builder()
                    .build()
                    .also {
                        it.setSurfaceProvider(binding?.pvPhotoFinder?.createSurfaceProvider())
                    }

            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                cameraProvider.unbindAll()

                cameraProvider.bindToLifecycle(
                        this, cameraSelector, preview)

            } catch (exc: Exception) {
                Log.e(TAG, "Use case binding failed", exc)
            }

            bindUseCases()

        }, ContextCompat.getMainExecutor(safeContext))

    }

    private fun takePhoto() {

        val imageCapture = imageCapture ?: return

        val photoFile = File(
                outputDirectory,
                SimpleDateFormat(FILENAME_FORMAT, Locale.US
                ).format(System.currentTimeMillis()) + ".jpg")

        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

        // been taken
        imageCapture.takePicture(
                outputOptions, ContextCompat.getMainExecutor(context), object : ImageCapture.OnImageSavedCallback {
            override fun onError(exc: ImageCaptureException) {
                Log.e(TAG, "Photo capture failed: ${exc.message}", exc)
            }

            override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                val savedUri = Uri.fromFile(photoFile)
                val msg = "Photo capture succeeded: ${photoFile.absolutePath}"
                viewModel.uploadOcrFile(photoFile.absolutePath)
                Log.d(TAG, msg)
            }
        })
    }

    private fun allPermissionsGranted() = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).all {
        ContextCompat.checkSelfPermission(safeContext, it) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera()
            } else {
                Toast.makeText(safeContext, "Permissions not granted by the user.", Toast.LENGTH_SHORT).show()
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun getOutputDirectory(): File {
        val mediaDir = activity?.externalMediaDirs?.firstOrNull()?.let {
            File(it, getString(R.string.app_name)).apply { mkdirs() }
        }
        return if (mediaDir != null && mediaDir.exists()) mediaDir else activity?.filesDir!!
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
    }

    private fun showSnackBar() {
        val snackbar = Snackbar.make(requireView(), getString(R.string.title_saved_to_library), Snackbar.LENGTH_LONG)
        snackbar.setAction(R.string.title_goto_library) { (activity as HomeActivity).navigateToLibrary() }
                .setBackgroundTint(resources.getColor(R.color.white))
                .setActionTextColor(resources.getColor(R.color.design_default_color_primary))
        snackbar.show()
    }

    companion object {
        const val TAG = "CaptureFragment"
        internal const val REQUEST_CODE_PERMISSIONS = 10
        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
    }

    override fun onItemSelected(t: ParagraphView?) {

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_save_to_library -> {
                showSnackBar()
                paragraphs?.let {
                    persistenceViewModel.persistScannedText(ScannedTextView(text = it.joinToString { pr -> pr.paragraph }, Date()))
                }
            }
        }
    }
}
