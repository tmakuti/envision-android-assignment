package dev.nashe.envisionandroidassignment.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import dev.nashe.envisionandroidassignment.databinding.ItemLibraryBinding
import dev.nashe.envisionandroidassignment.model.ScannedTextView
import dev.nashe.envisionandroidassignment.ui.adapters.base.BaseRecyclerAdapter
import dev.nashe.envisionandroidassignment.ui.viewholder.LibraryViewHolder

class LibraryAdapter(callback: Callback<ScannedTextView>) :
        BaseRecyclerAdapter<ScannedTextView, LibraryViewHolder>(callback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LibraryViewHolder {
        return LibraryViewHolder(ItemLibraryBinding.inflate(LayoutInflater.from(parent.context)))
    }
}