package dev.nashe.envisionandroidassignment.ui.viewholder

import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import dev.nashe.envisionandroidassignment.databinding.ItemLibraryBinding
import dev.nashe.envisionandroidassignment.model.ScannedTextView
import dev.nashe.envisionandroidassignment.ui.adapters.base.BaseRecyclerAdapter
import dev.nashe.envisionandroidassignment.ui.viewholder.base.BaseViewHolder

class LibraryViewHolder(binding: ItemLibraryBinding) :
        BaseViewHolder<ScannedTextView, ItemLibraryBinding, BaseRecyclerAdapter.Callback<ScannedTextView>>(
                binding
        ) {

    override fun doOnBind(item: ScannedTextView, callback: BaseRecyclerAdapter.Callback<ScannedTextView>?) {
        binding.item = item
        Log.d("ScannedTextViewModel", "Setting ViewHolder")


        val rootView = binding.root
        rootView.layoutParams = RecyclerView.LayoutParams(
                RecyclerView.LayoutParams.MATCH_PARENT,
                RecyclerView.LayoutParams.WRAP_CONTENT
        )

        if (callback != null) {
            binding.root.setOnClickListener { callback.onItemSelected(item) }
        }
    }
}