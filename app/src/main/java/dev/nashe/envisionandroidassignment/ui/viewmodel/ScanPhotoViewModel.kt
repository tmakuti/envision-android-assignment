package dev.nashe.envisionandroidassignment.ui.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.nashe.domain.interactors.UploadFile
import dev.nashe.envisionandroidassignment.mapper.ParagraphViewMapper
import dev.nashe.envisionandroidassignment.model.ParagraphView
import dev.nashe.envisionandroidassignment.utils.Result
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ScanPhotoViewModel @Inject constructor(
    private val uploadFile: UploadFile,
    private val paragraphViewMapper: ParagraphViewMapper
) : ViewModel() {

    private val _detectedTextLiveData = MutableLiveData<Result<List<ParagraphView>>>()
    val detectedTextLiveData: LiveData<Result<List<ParagraphView>>>
        get() = _detectedTextLiveData

    fun uploadOcrFile(path : String){
            viewModelScope.launch {
                _detectedTextLiveData.value = Result.Loading
                try {
                    val scannedTextListing = paragraphViewMapper.mapToViewList(uploadFile(path))
                    _detectedTextLiveData.value = Result.Success(scannedTextListing)
                } catch (e: Exception) {
                    Log.d("ScanViewModel", e.message.toString())
                    _detectedTextLiveData.value = Result.Error(e.message)
            }
        }
    }
}