package dev.nashe.envisionandroidassignment.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import dev.nashe.envisionandroidassignment.R
import dev.nashe.envisionandroidassignment.databinding.FragmentLibraryBinding
import dev.nashe.envisionandroidassignment.model.ScannedTextView
import dev.nashe.envisionandroidassignment.ui.adapters.LibraryAdapter
import dev.nashe.envisionandroidassignment.ui.adapters.base.BaseRecyclerAdapter
import dev.nashe.envisionandroidassignment.ui.fragments.base.BaseFragment
import dev.nashe.envisionandroidassignment.ui.viewmodel.ScannedTextViewModel
import dev.nashe.envisionandroidassignment.utils.Result

@AndroidEntryPoint
class LibraryFragment : BaseFragment<FragmentLibraryBinding>(), BaseRecyclerAdapter.Callback<ScannedTextView> {
    override val layout: Int
        get() = R.layout.fragment_library

    private val viewModel : ScannedTextViewModel by activityViewModels ()
    private val libraryAdapter : LibraryAdapter = LibraryAdapter(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeLibraryListing()

        binding?.apply {
            initRecyclerView()
        }
    }

    private fun observeLibraryListing(){
        viewModel.scannedTextLiveData.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Loading -> {

                }
                is Result.Success -> {
                    libraryAdapter.setItems(it.data)
                }
                is Result.Error -> {

                }
                is Result.Idle -> {

                }
            }
        })
    }

    private fun initRecyclerView() {
        binding!!.rvLibrary.apply {
            adapter = libraryAdapter
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }
    }

    override fun onItemSelected(t: ScannedTextView?) {
        Toast.makeText(context, t?.text, Toast.LENGTH_LONG).show()
    }
}