package dev.nashe.envisionandroidassignment.mapper.base

interface DomainMapper<out D, in V> {
    fun mapToDomain(view: V): D
}