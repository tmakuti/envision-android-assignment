package dev.nashe.envisionandroidassignment.mapper

import dev.nashe.domain.models.text.ScannedText
import dev.nashe.envisionandroidassignment.mapper.base.ViewMapper
import dev.nashe.envisionandroidassignment.model.ScannedTextView
import javax.inject.Inject

class ScannedTextViewMapper @Inject constructor() : ViewMapper<ScannedTextView, ScannedText> {
    override fun mapToView(domain: ScannedText): ScannedTextView {
        return ScannedTextView(
            text = domain.text,
            savedAt = domain.savedAt
        )
    }
}