package dev.nashe.envisionandroidassignment.mapper

import dev.nashe.domain.models.text.Paragraph
import dev.nashe.envisionandroidassignment.mapper.base.ViewMapper
import dev.nashe.envisionandroidassignment.model.ParagraphView
import javax.inject.Inject

class ParagraphViewMapper @Inject constructor() : ViewMapper<ParagraphView, Paragraph> {
    override fun mapToView(domain: Paragraph): ParagraphView {
        return ParagraphView(
            language = domain.language,
            paragraph = domain.paragraph
        )
    }
}