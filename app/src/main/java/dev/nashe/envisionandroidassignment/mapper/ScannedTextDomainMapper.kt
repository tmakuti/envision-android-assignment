package dev.nashe.envisionandroidassignment.mapper

import dev.nashe.domain.models.text.ScannedText
import dev.nashe.envisionandroidassignment.mapper.base.DomainMapper
import dev.nashe.envisionandroidassignment.model.ScannedTextView
import javax.inject.Inject

class ScannedTextDomainMapper @Inject constructor() : DomainMapper<ScannedText, ScannedTextView> {
    override fun mapToDomain(view: ScannedTextView): ScannedText {
        return ScannedText(
            text = view.text,
            savedAt = view.savedAt
        )
    }
}