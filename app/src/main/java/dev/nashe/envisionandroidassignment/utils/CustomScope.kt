package dev.nashe.envisionandroidassignment.utils

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob


fun CustomScope(dispatcher: CoroutineDispatcher = Dispatchers.Unconfined): CoroutineScope =
        CoroutineScope(SupervisorJob() + dispatcher)