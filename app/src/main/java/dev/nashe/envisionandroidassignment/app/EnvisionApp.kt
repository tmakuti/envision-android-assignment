package dev.nashe.envisionandroidassignment.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class EnvisionApp : Application() {

}